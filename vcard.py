#!/usr/bin/env python
# coding: utf-8

import vobject
import csv
import os
import sys
import phonenumbers

csvfile = os.path.realpath(sys.argv[1])
csv_reader = csv.reader(open(csvfile).readlines())
# skip header row
csv_reader.next()

for row in csv_reader:
		first_name,last_name = row[0].split()
		j = vobject.vCard()
		o = j.add('fn')
		o.value = row[0]

		o = j.add('n')
		o.value = vobject.vcard.Name( family=last_name, given=first_name )

		o = j.add('categories')
		o.value = ["Vietnam", "Lazada"]

		o = j.add('org')
		o.value = ["Lazada"]

		if len(row) > 1 and row[1]:
				o = j.add('tel')
				o.type_param = "cell"
				o.value =phonenumbers.format_number(phonenumbers.parse(row[1], "VN"),
								phonenumbers.PhoneNumberFormat.INTERNATIONAL)

		if len(row) > 2 and row[2]:
				o = j.add('email')
				o.value = row[2]

		if len(row) > 3 and row[3]:
				o = j.add('skype')
				o.type_param = 'home'
				o.value = row[3]

		if len(row) > 4 and row[4]:
				o = j.add('skype')
				o.type_param = 'work'
				o.value = row[4]

		if not os.path.exists("vcards"):
				os.mkdir("vcards")
		f = open(os.path.join("vcards", row[0]+".vcard"), "w+")


		f.write(j.serialize())
		f.close()



